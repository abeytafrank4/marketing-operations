## Marketing Operations Team  

- **Director**: [Dara Warde](https://gitlab.com/darawarde)
- **Senior Marketing Operations Manager**: [JJ Cordz](https://gitlab.com/jjcordz) 
     - **Focus**: System Architect 
- **Marketing Operations Manager**: [Nichole LaRue](https://gitlab.com/nlarue)
     - **Focus**: XDR liason and process improvements
- **Marketing Operations Manager**: [Robert Kohnke](https://gitlab.com/rkohnke)
     - **Focus**: Reporting, Analytics & Dashboards 
- **Associate Marketing Operations Manager**: [Jameson Burton](https://gitlab.com/jburton)


## Working with Marketing Operations  

### If its not in an **issue**, its not our issue! 

Global Issue Board - https://gitlab.com/groups/gitlab-com/-/boards/825719  

Labels to use:

- `MktgOps`: Issue initially created, used in templates, the starting point for any label that involved Mktg OPS
- `MktgOps - FYI`: Issue is not directly related to operations, no action items for Mktg OPS but need to be aware of the campaign/email/event
- `MktgOps - Reporting`: Reporting request for MktgOps
- `MktgOps - List Import`: Used for list imports of any kind - event or ad hoc
- `MktgOps-Priority::1 - Top Priority`: Issue that is related to a breaking change, OKR focus, any other prioritized project by Mktg OPS leadership. This category will be limited because not everything can be a priority.
- `MktgOps-Priority::2 - Action Needed`: Issue has a specific action item for Mktg OPS to be completed with delivery date 90 days or less from issue creation date. This tag is to be used on projects/issues not owned by Mktg OPS (example: list upload).
- `MktgOps-Priority::3 - Future Action Needed`: Issue has a specific action item for Mktg OPS, the project/issue is not owned by Mktg OPS and delivery or event date is 90 days or more from issue creation.
- `MktgOps::1 - Planning`: Issues that are currently being scoped/considered but are not being actively worked on.
- `MktgOps::2 - On Deck`: Issues that have been scoped/considered and will be added to the In Process queue next. 
- `MktgOps::3 - In Process`: Issues that are actively being worked on in the current week/sprint (week? Month? Two-week period?)
- `MktgOps::4 - UAT`: Issues that MktgOps has completed its required tasks for and is ready for review and approval by the Requester/Approver.
- `MktgOps::5 - On Hold`: Issue that is not within existing scope of Mktg OPS current targets, blocked by MktgOps-related task/issue, or external (non-GitLab) blocker.
- `MktgOps::6 - Blocked`: Issue that is currently being worked on by Mktg Ops and at least one other team wherein Mktg Ops is waiting for someone else/another team to complete an action item before being able to proceed.
- `MktgOps::7 - Completed`: MktgOps has completed their task on this issue although the issue may not be closed