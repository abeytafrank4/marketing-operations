### :calendar: Week of XX Month 2019


# Agenda 

When adding agenda items please out it under your functionality with your initials next to it and then action item.     

 :exclamation: Example: 
Field Marketing:
* [ ]   EW - DISCUSS: Post event follow up routing

________________________________________________________________________________

### Digital Marketing
* [ ] Item 1   
* [ ] Item 2  
* [ ] Item 3 

### xDR
* [ ] Item 1   
* [ ] Item 2  
* [ ] Item 3 


### Field Marketing
* [ ] Item 1   
* [ ] Item 2  
* [ ] Item 3 

### MktgOPS -> RevMktg 
* [ ] Item 1  
* [ ] Item 2 
* [ ] Item 3

________________________________________________________________________________

### :white\_check\_mark: Next Steps / Action Items for the Week  

* [ ] Item 1   
* [ ] Item 2  
* [ ] Item 3 


### :repeat: Open Items from previous Meetings   

* [ ] Item 1   
* [ ] Item 2  
* [ ] Item 3
* [ ] Item 4
* [ ] Item 5





/label ~"MktgOps" ~"Digital Marketing Programs" ~"RevMktg - Attention" ~SDR ~"status::plan"
/assign @jjcordz @ewhelchel @darawarde
/cc @lblanchard @amimmo @Phuynh @megan_odowd @jgragnola
/epic <&205>