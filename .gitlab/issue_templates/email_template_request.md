## Purpose   

Define the purpose/goal of the email template.   

**Example**: Event invitation with hero image & CTA button

## :email: Layout  

Provide examples of email layouts you like 


:question: Please submit any questions about this template to the #mktgops slack channel.   

/label ~MktgOps ~"MktgOps-Priority::2 \- Action Needed" ~"status::plan" 