### Systems to Review 
 
When the system has been reviewed please **check** the box & add the details to the appropriate table below. 


**SalesOPS**
* [ ] Salesforce  
* [ ] Chorus
* [ ] Clari
* [ ] Datafox  

**MktgOPS**  
* [ ] Outreach
* [ ] DiscoverOrg
* [ ] Marketo 
* [ ] Moz
* [ ] Sprout Social
* [ ] Drift
* [ ] LinkedIn Sales Navigator
* [ ] FunnelCake  
* [ ] LeanData
* [ ] DemandBase

### Planned Hiring   

| Role | Forecast in Quarter | 
| :--- | :----- | 
| Sales - Strategic Account Leader |  | 
| Sales - Commercial: Mid-Market |  | 
| Sales - Commercial: Small Business |  |
| Sales - Area Sales Manager |  |
| Sales - Customer Success |  |
| Sales - Operations |  | 
| Sales - Other (not accounted for above) |  | 
| Marketing - SDR |  |
| Marketing - Operations |  |
| Marketing - Field Marketing |  |
| Marketing - Marketing Programs |  | 
| Marketing - Digital Marketing |  |
| Marketing - Content |  |
| Marketing - Corporate |  |
| Marketing - Other (not accounted for above) |  |

### Seats to be Added    

| Tool | Current (open / total) | Need to Add | 
| :--- | :----------: | :-----: |
| Salesforce |   |  |
| Chorus - Recorder |  |  |
| Chorus - Listener |  |  | 
| Clari |  |  | 
| Datafox |  |  | 
| Outreach |   |  | 
| DiscoverOrg |   |  |
| Marketo |  |  | 
| Moz |   |  | 
| Sprout Social |   |  |
| Drift |   |  |
| LinkedIn Sales Nav |   |  | 
| FunnelCake |  |  | 
| LeanData |  |  |
| DemandBase |  |  |






/cc @darawarde @dhong @zgaston
/label ~"status::plan" ~"MktgOps::1 - Planning" ~SalesOPS 