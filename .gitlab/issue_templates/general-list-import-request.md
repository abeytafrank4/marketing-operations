## :notepad\_spiral: List Details    

:eight\_spoked\_asterisk:  List Origin **required**:     
:eight\_spoked\_asterisk:  SFDC Campaign: `if part of a campaign or event outreach please specify`   
:eight\_spoked\_asterisk:  Outreach Tag: `LastName - EventName`   
:eight\_spoked\_asterisk:  Google Sheet (make sure you have turned on 'sharing', if locked will delay acceptance):    

### :arrow\_right: **SLA**: Once the file has accepted, there is up to a five (5) business day turnaround time for list imports. :arrow\_left:<br>&nbsp;&nbsp;&nbsp;MktgOPS will do cursory check of list AND accept or reject list w/in 24 business hours of issue creation. 


### :pushpin: Required Fields   
- `First Name`
- `Last Name`
- **Source** - in EMEA only GDPR compliant sources are allowed to be imported and prospected to
- `Email` - records without an email address **will not** be imported  
- Address fields  - `Street`, `City`, `State`, etc need to be in their own individual column

## 🛁 List clean step 1 (**`Requestor`**)

* [ ]  Use [template](https://docs.google.com/spreadsheets/d/1INbR1I-2REF1n4C7XVatdEft9lQjBp3zrWo2cMnvN-Q/edit#gid=1927309383) to copy/paste columns and alert to any email syntax errors, duplicates, and clean up Proper Capitalization
* [ ]  Remove duplicates based on template alerts and correct errors in emails, as displayed
* [ ]  Link to ready-to-upload googledoc of list in issue comments and tag Marketing Ops

## ✅  List accepted by MktgOPS

* [ ]  List accepted - OPS will respond w/in 24 business hours from issue creation if list has been accepted

## 🔼 Upload list to Salesforce (MktgOPS)

* [ ]  Upload list to SFDC
* [ ]  Notify in issue when list is uploaded


### :question: Questions & FAQ  
- See [List Import](https://about.gitlab.com/handbook/business-ops/#list-imports) section in handbook for further clarification.   
- Submit any questions about this template to the #mktgops slack channel.

/label ~MktgOps ~"MktgOps - List Import" ~"status::plan" 
/cc @jjcordz @nlarue