:exclamation: This template should only be used when you will be out of office for more than 24hours.   

### Person Information

| | |
|:---|:---|
| **Name** | `Full Name` |
| **GitLab Email** | `Email address` | 
| **Title** | `Full Title` | 
| **Manager** | `Report to Name` |


### Vacation / Out of Office  

The **start date** is the *first* day you will be out of the office. The **end date** is the *first* day you will be back in the office. 

| | |
|:---|:---|
| Start Date | YYYY-MM-DD | 
| End Date | YYYY-MM-DD |


/label ~LeanData ~"status::plan" ~"MktgOps::1 - Planning" 
/assign @jjcordz 