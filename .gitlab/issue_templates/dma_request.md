:exclamation: Title this issue `DMA Request - REGION - City/Area`  

**Requestor**:    
**Region**: (`AMER - West`, `AMER - East`, `AMER - PubSec`, `EMEA`, `APAC`)      
**Area to be included**: <! Please provide the geographic area that you'd like included in the DMA list>       


### Ops Tasks  

- [ ] Update the handbook with new DMA in appropriate `Region` &/or `Sub-Region`


The existing list of DMA list can be viewed: https://about.gitlab.com/handbook/marketing/marketing-operations/marketo#geographic-dma-list    

/label ~MktgOps ~"status::plan" 
