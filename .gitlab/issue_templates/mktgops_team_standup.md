## :calendar: Week of XX Month 2019

### :beach: Out of Office  

Please be sure to set your email auto-responder if you are going to be out for more than a day. Utilize [PTO Ninja in Slack](https://about.gitlab.com/handbook/paid-time-off/#pto-ninja) to notify of your absence when `@mention` and by default set another OPS team member as your coverage.   

- **Dara**: 
- **Nichole**: 
- **Robert**: 
- **Jameson**:
- **JJ**: 


### :goal: Focus Item(s) for the Week

- **Dara**:
- **Nichole**: 
- **Robert**:
- **Jameson**:
- **JJ**:

### :snowflake: Holiday 2019

- **Dara**: 
- **Nichole**:
- **Robert**:
- **Jameson**:
- **JJ**:

### :pushpin: Agenda - Monday 

:exclamation: When adding agenda items please put your initial next to it and then action item.     

Example: `JJ - DISCUSS: how we organize issues`     

#### :question: True Standup - What did you do yesterday? What are you doing today? Where are you blocked? 

-   
-  
-  


### :pushpin: Agenda - Wednesday 

#### :question: True Standup - What did you do yesterday? What are you doing today? Where are you blocked? 

-   
-  
-  


### :pushpin: Agenda - Friday 

#### :question: True Standup - What did you do yesterday? What are you doing today? Where are you blocked? 

-   
-  
-  

### :white\_check\_mark: Next Steps / Action Items for the Week  

* [ ] Item 1   
* [ ] Item 2  
* [ ] Item 3 


### :repeat: Open Items from previous Meetings   

* [ ] Item 1   
* [ ] Item 2  
* [ ] Item 3
* [ ] Item 4
* [ ] Item 5


#### :lock: Quick Link to [Internal only doc](https://docs.google.com/document/d/1xaHbv5kOFM8Rf32srEDqIAqwd1wbEgCf4Ug696ZjUCE/edit#heading=h.4wcii92f1c6j) for `confidential` topics




/label ~"MktgOps::1 - Planning" ~"status::plan" 
/assign @jjcordz @rkohnke @nlarue @darawarde @jburton
/epic <&157>

