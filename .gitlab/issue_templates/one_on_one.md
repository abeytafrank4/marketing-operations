## :calendar: DAY of Meeting (XX Month 2019)

### :pushpin: Agenda   

:exclamation: When adding agenda items please put your initial next to it and then action item.     

Example: `JJ - DISCUSS: how we organize issues`

-   
-  
-


### :white\_check\_mark: Next Steps / Action Items for the Week  

* [ ] Item 1   
* [ ] Item 2  
* [ ] Item 3 


### :repeat: Open Items from previous Meetings   

* [ ] Item 1   
* [ ] Item 2  
* [ ] Item 3
* [ ] Item 4
* [ ] Item 5


### Summary  

< !Add details to wrap up any open action items >  


#### :lock: Quick Link to [Internal only doc](!Add Relevant Doc) for `confidential` topics




/label ~"status::plan" ~"MktgOps::1 - Planning" 
/assign @darawarde
/epic <&158>

