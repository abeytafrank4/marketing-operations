```
<!---
This issue is for requesting a new report/dashboard within either Periscope or Salesforce.
--->
```

#### What is the business question you are trying to answer or what is the marketing metric you want puller/visualized? 

Example: How many records (lead and contacts) become MQLs per month, that have attended a live event? 

##### What is the impact of this question and how will it help the marketing org or your specific marketing team?


#### Please link to where this (or these) performance indicator/s are defined in the handbook. 

Everything needs to be defined in the handbook.

#### Who will be using this data?

Example: Field Marketing Managers want to know which type of event generates the most Opportunities. 

#### What time frames are crucial here? 

Example: I would like to look at conversions by month, for the current and previous Fiscal Quarter.

#### Will this deliver business value within 90 days?

If not, consider why you want this data.

#### What is the visualization you are trying to create?

Include any links or screenshots, if appropriate. As a rule of the thumb, there are 10 standard visualization types currently used by marketing. They are:

1. Simple Text
2. Table
3. Heat map (Table with Conditional Highlighting)
4. Scatterplot
5. Line graph
6. Slope graph
7. Vertical bar chart
8. Stacked vertical bar chart
9. Horizontal bar chart
10. Stacked horizontal bar chart

#### What specific fields, if any, do you know you want included or filtered by?
*Note: It is most helpful if these are either hyperlinked SFDC fields or defined fields from the handbook.
**Examples**: [Region/Vertical](https://about.gitlab.com/handbook/business-ops/#regionvertical) or [MQL](https://about.gitlab.com/handbook/business-ops/#mql-definition)
 
 
#### What interactions/drill downs are required?

Example: I'd like to be able to dig into the specific opportunity details (owner, account owner, IACV). I'd also like to be able to filter by region. 

#### Any caveats or details that may be helpful?


:question: Please submit any questions about this template to the #mktgops slack channel.   

/label ~"status::plan" ~"MktgOps - Reporting" ~"MktgOps::1 - Planning" 
/assign @rkohnke