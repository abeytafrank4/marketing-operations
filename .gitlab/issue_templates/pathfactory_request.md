## Purpose

This issue will track the request for admin-level PathFactory modification requests.

## ✔️ Check the appropriate request below:

* [ ]  New form request
* [ ]  New theme creation
* [ ]  New CTA
* [ ]  New content tag (must be approved by Sarah or Erica)
* [ ]  New content type (must be approved by Sarah or Erica)
* [ ]  Other (please describe below)

## 📝 Please fill out the description information for the request:





Please submit any questions about this template to the #mktgops slack channel.

/label ~"MktgOps::1 - Planning"  ~"MktgOps-Priority::2 \- Action Needed" ~"PathFactory" ~"status::plan"
/assign @nlarue