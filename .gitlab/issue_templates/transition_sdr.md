:exclamation: Title of the issue should be `Full Name` - `ISO Date YYYY-MM-DD` - Transition

**Name**: <INSERT TEAM MEMBER NAME>  
**Old position**:   
**New position**:   
**Effective date of transition**: `FILL IN - ISO Date YYYY-MM-DD`  
**New Manager** (who do they report to):

**Who is taking over old responsibilities**: `FILL IN`

## For SDR transition

- `<INSERT NAME>` to replace `<INSERT NAME>` as `<FORMER ROLE>` for the `<REGION>` team.
- `<INSERT NAME>` will work in the `<INSERT TERRITORY>` (refer to Territory maps in [Sales handbook](/handbook/sales/territories))

### ToDos

* [ ] Transition all records owned by `<INSERT NAME>` to `<NEW PERSON NAME>` 
* [ ] Remove from Territory table replace with `<INSERT NAME>` 
* [ ] Modify `SDR` field in SFDC on the ACCOUNTS owned by `<INSERT SAL>`
* [ ] Modify LeanData routing to remove from Round Robin groups   
* [ ] Remove from Drift & other BDR specific tools
* [ ] Update profile username in SFDC by appending a number before the `@`
* [ ] Create a new users in SFDC for their new role with appropriate permissions etc. - make sure their username and email are both their normal email 
* [ ] Notify Sales Commission Manager in regards to the new user creation 
* [ ] After all updates in SFDC - transition old user seats over to Salesforce Platform User Licenses
* [ ] After All updates in SFDC - deactivate the SDRs picklist values in the SDR 2 global picklist
* [ ] Update Outreach team   
* [ ] Modify the tags in LISN to reflect the correct team  
* [ ] Review (and update) the Google alias groups & Slack groups to reflect the change 
* [ ] Work with PeopleOPS to ensure the other systems are updated appropriately esp BambooHR so the integration to Okta is correct  
* [ ] Review work that is in-flight w/in Sequences & determine how to handle (Operationally we can change "sender" &/or we can schedule a 'warm' handoff email) 



/assign @jjcordz
/label ~MktgOps ~"status::plan" 