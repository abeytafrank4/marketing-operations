### 📝 [Epic Link >>](#)

## Purpose

This issue will track the list cleanup and upload for events. Adherence to timelines and receiving lists as soon as possible from event organizers is critical to a timely follow up.

**Below to be completed by MktgOPS, organized by Marketing Programs and with support of Field Marketing as liaison with event organizers.**

## 🛁 List clean step 1 (campaign owner)

* [ ] Use [template](https://docs.google.com/spreadsheets/d/1INbR1I-2REF1n4C7XVatdEft9lQjBp3zrWo2cMnvN-Q/edit#gid=1927309383) to copy/paste columns and alert to any email syntax errors, duplicates, and clean up Proper Capitalization
* [ ] Remove duplicates based on template alerts and correct errors in emails, as displayed
* [ ] Work with event booth staff to clean and take notes on conversations
* [ ] Translate international lead lists as necessary - campaign owner to delegate, if relevant
* [ ] Link to ready-to-upload googledoc of list in issue comments and tag Marketing Ops

## 🔼 Upload list to Salesforce & notify sales development (MktgOPS)

* [ ] Upload list to Marketo
* [ ] Run workflow to modify `Status` = `Inquiry` IF `Follow Up Requested`
* [ ] Modify Bizible Touchpoints to Day 1 of related event
* [ ] Notify in issue when list is uploaded
* [ ] Lead list created for campaign and shared with SDRs
* [ ] Import to Outreach - apply tag matching Campaign Name

### :clock: List imports will be completed within 24 HOURS after receiving the list from Field Marketing. 

Please submit any questions about this template to the #mktgops slack channel.

/label ~"Marketing Programs" ~"status::plan" ~"MktgOps - List Import" ~"MktgOps::1 - Planning"